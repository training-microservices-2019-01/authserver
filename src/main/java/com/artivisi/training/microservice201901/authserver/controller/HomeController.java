package com.artivisi.training.microservice201901.authserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired private PasswordEncoder passwordEncoder;

    @GetMapping("/home")
    public ModelMap homepage(Authentication currentUser){
        System.out.println("BCrypt [test123] : "+passwordEncoder.encode("test123"));
        return new ModelMap()
                .addAttribute("currentUser", currentUser);
    }
}
