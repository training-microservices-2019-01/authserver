# Authorization Server #

Grant Type Flow :

* Authorization Code
* Implicit

## Authorization Code Grant Type ##

1. Mendapatkan `authorization code`. Browse ke [http://localhost:8080/oauth/authorize?client_id=clientwebbased&grant_type=authorization_code&response_type=code](http://localhost:8080/oauth/authorize?client_id=clientwebbased&grant_type=authorization_code&response_type=code)

2. Setelah login, user akan diredirect sesuai dengan `redirect_uri` yang didaftarkan untuk `client001`. Misalnya hasilnya seperti ini:

        http://localhost:10000/handle-oauth-callback?code=NCdkfb
    
    Authorization code : `NCdkfb`

3. Tukarkan authorization code menjadi `access_token`. Caranya : request `POST` ke [http://localhost:8080/oauth/token](http://localhost:8080/oauth/token) dengan spesifikasi :

    * Basic Authentication : client id dan client secret
    
    * Request Parameter :
    
        * client_id
        * grant_type
        * code

    Dengan Postman seperti ini
        
    [![Postman Get Token](docs/01-postman-get-token.png)](docs/01-postman-get-token.png)

    Hasilnya seperti ini
    
    ```json
    {
        "access_token": "92c3e776-d440-4a9e-b60b-a8b61e09768d",
        "token_type": "bearer",
        "refresh_token": "243a70ce-c1fb-4cd9-9568-b0510f7297bf",
        "expires_in": 43199,
        "scope": "review_transaksi approve_transaksi"
    }
    ```

4. Periksa apakah `access_token` valid. Lakukan `POST` ke [http://localhost:8080/oauth/check_token](http://localhost:8080/oauth/check_token) dengan spesifikasi:

    * Basic Authentication
    
    * Request Parameter:
    
        * `token` : `92c3e776-d440-4a9e-b60b-a8b61e09768d`
    
    Dengan Postman seperti ini
            
    [![Postman Check Token](docs/02-postman-check-token.png)](docs/02-postman-check-token.png)
                
    Hasilnya seperti ini
    
    ```json
    {
        "aud": [
            "belajar"
        ],
        "user_name": "user001",
        "scope": [
            "review_transaksi",
            "approve_transaksi"
        ],
        "active": true,
        "exp": 1544209904,
        "authorities": [
            "VIEW_TRANSAKSI"
        ],
        "client_id": "client001"
    }
    ```